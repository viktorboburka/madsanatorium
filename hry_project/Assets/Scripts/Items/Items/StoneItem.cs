﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneItem : Item
{
    public override bool Use() {
        //spawn rock
        GameObject rock = Instantiate(Resources.Load("Prefabs/Items/Stone") as GameObject, LogicMgr.Instance.GetPlayer().flashlight.GetFlashlightPosition() - new Vector3(0,0,0.5f), Quaternion.identity);
        //add force
        rock.GetComponent<Rigidbody>().AddForce(LogicMgr.Instance.GetPlayer().flashlight.GetFlashlightOrientation() * 20.0f, ForceMode.Impulse);
        return true;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
