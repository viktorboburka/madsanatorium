﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField]
    float distance = 10;

    Transform player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").transform;
        transform.position = new Vector3(player.position.x, player.position.y, -distance);
    }

    // Follows player
    void Update()
    {
        Vector3 lookPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 offset = (lookPosition - player.position).normalized * 2;
        transform.position = Vector3.Lerp(transform.position, new Vector3(player.position.x + offset.x, player.position.y + offset.y, player.position.z -distance), Time.deltaTime * 2);
    }
}
