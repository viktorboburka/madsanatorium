﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PlayerControls), typeof(PlayerMovement), typeof(PlayerFlashlight))]
public class Player : MonoBehaviour
{
    public PlayerFlashlight flashlight { get; private set; }
    public PlayerControls controls { get; private set; }
    public PlayerMovement movement { get; private set; }

    public Inventory inventory;

    [HideInInspector]
    public GameObject lastLadderUsed { get; set; }

    UnityAction deathAction;
    // Start is called before the first frame update
    void Start()
    {
        flashlight = GetComponent<PlayerFlashlight>();
        controls = GetComponent<PlayerControls>();
        movement = GetComponent<PlayerMovement>();

        //NOT NEEDED AFTER DEVELOPMENT
        Material mat = Resources.Load("Materials/PlayerMatTransparent") as Material;
        mat.color = new Vector4(1, 1, 1, 1);
    }    

    public void Death(UnityAction completion) {
        deathAction = completion;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;        
        GetComponent<Animator>().Play("Death");
    }

    public void DeathEnd() {
        deathAction();
    }
}
