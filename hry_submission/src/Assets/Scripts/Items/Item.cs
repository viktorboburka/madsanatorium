﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]
public abstract class Item : MonoBehaviour
{
    [SerializeField]
    Sprite icon;

    public string name;

    //return true if player can use it, otherwise returns false
    public abstract bool Use();
    // Start is called before the first frame update

    public Sprite GetIcon() {
        return icon;
    }

    //Removes item from world
    public void Deactivate() {
        transform.GetChild(0).gameObject.SetActive(false);
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<BoxCollider>().enabled = false;
    }
    void Start()
    {
            
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
