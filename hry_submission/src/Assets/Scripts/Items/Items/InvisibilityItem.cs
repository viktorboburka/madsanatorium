﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibilityItem : Item
{

    public override bool Use()
    {

        GetComponent<ItemAudio>().playSound();
        return LogicMgr.Instance.GetPlayer().movement.useInvisibility();        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
