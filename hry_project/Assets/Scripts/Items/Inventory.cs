﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    List<Item> items;
    int selectedItem = -1;
    // Start is called before the first frame update
    void Start()
    {
        items = new List<Item>();
        Events.EventsMgr.Instance.StartListening(new Events.ItemCollected(), AddItemToInventory);
    }

    public void AddItemToInventory(Events.EventType eventType) {
        Item item = (eventType as Events.ItemCollected).item;
        items.Add(item);
        GameObject newItem = Instantiate(Resources.Load("Prefabs/UI/ItemUI") as GameObject, transform);
        newItem.GetComponent<Image>().sprite = item.GetIcon();
        newItem.GetComponent<RectTransform>().sizeDelta = new Vector2(60,60);
        newItem.transform.GetChild(0).GetComponent<Text>().text = "";        
    }


    public void SelectNextItem() {
        if(selectedItem + 1 < items.Count) {
            SelectItem(selectedItem + 1);
        }
        else if(items.Count >1){
            SelectItem(0);
        }
    }

    public void DeselectCurrentItem() {
        DeselectItem(selectedItem);
    }
    void DeselectItem(int n) {
        transform.GetChild(n).Find("Border").GetComponent<Image>().color = Color.white;
        transform.GetChild(n).GetChild(0).GetComponent<Text>().text = "";
        selectedItem = -1;
    }
    public void SelectItem(int n) {
        if(n < items.Count) {
            if (n == selectedItem) {
                DeselectItem(selectedItem);
            }
            else {
                if(selectedItem >= 0)
                    DeselectItem(selectedItem);
                selectedItem = n;
                transform.GetChild(selectedItem).Find("Border").GetComponent<Image>().color = Color.green;
                transform.GetChild(selectedItem).GetChild(0).GetComponent<Text>().text = items[selectedItem].name;
            }
        }
    }

    public bool IsItemSelected() {
        return selectedItem >= 0;
    }
    public void UseSelectedItem() {
        if (items[selectedItem].Use()) {
            items.RemoveAt(selectedItem);
            Destroy(transform.GetChild(selectedItem).gameObject);
            selectedItem = -1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
