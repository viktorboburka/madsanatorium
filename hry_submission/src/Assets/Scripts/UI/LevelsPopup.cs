﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelsPopup : Popup
{
    [SerializeField]
    Button backButton;

    [SerializeField]
    Transform levelsPanel;
    // Start is called before the first frame update
    void Start()
    {
        backButton.onClick.AddListener(Close);
    }

    //This popup is located in main menu, you can select what level to play, levels are unlocked after completing
    public void ShowLevels(int maxLevel) {
        TextAsset txt = (TextAsset)Resources.Load("Data/levels", typeof(TextAsset));
        string content = txt.text;
        JSONObject levelsJson = new JSONObject(content);
        for(int i = 1; i <= maxLevel;i++) {
            GameObject level = Instantiate(Resources.Load("Prefabs/UI/LevelButton") as GameObject, levelsPanel);
            level.GetComponent<LevelButton>().Init(levelsJson[i.ToString()], i);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
