﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    float speedMultiplier = 0.06f;
    float xSpeed;  //speed
    bool stealthed = false;  //is in stealth mode
    bool invisible = false; //is invisible
    bool front = true;  //moving forward
    [HideInInspector]
    public bool stationary = true; //is standing still
    bool fast = false;  //speed potion effect on
    [HideInInspector]
    public bool isClimbing = false;  //is climbing ladder

    AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

   
    void Update()
    {
        if (LogicMgr.Instance.IsPaused() || LogicMgr.Instance.IsGameOver() || LogicMgr.Instance.IsRotating() || isClimbing) {
            return;
        }
        xSpeed = Input.GetAxis("Horizontal");
        if (xSpeed == 0) stationary = true;
        else
            stationary = false;
        if (xSpeed < 0 && front) {
            front = false;
            }

        if (xSpeed > 0 && !front)
        {
            front = true;
        }

        if (stealthed) {
            xSpeed /= 2.0f;
        }
        xSpeed = xSpeed * speedMultiplier;
        if(Mathf.Sign(xSpeed) != Mathf.Sign(LogicMgr.Instance.GetPlayer().flashlight.direction)) {
            xSpeed /= 1.5f;
        }
        transform.position += new Vector3(xSpeed, 0, 0);
    }
    
    public void SetClimbing(bool isOn) {
        isClimbing = isOn;
    }

    public bool getStealthed()
    {
        return stealthed;
    }

    public bool getInvisible()
    {
        return invisible;
    }

    public float getxSpeed()
    {
        return xSpeed;
    }

    public void TurnStealth(bool isOn) {
        if(stealthed != isOn) stealthed = isOn;
    }

    public bool useInvisibility()
    {
        if (!invisible) {
            StartCoroutine(InvisibilityPotion());
            return true;
        }
        return false;
    }

    public bool useSpeed()
    {
        if (!fast) {
            StartCoroutine(SpeedPotion());
            return true;
        }
        return false;
    }

    IEnumerator SpeedPotion() {
        fast = true;
        speedMultiplier *= 1.5f;
        yield return new WaitForSeconds(3);
        fast = false;
        speedMultiplier /= 1.5f;
        yield return null;
    }

    IEnumerator InvisibilityPotion() {
        Material mat = transform.Find("Engineer").GetComponent<SkinnedMeshRenderer>().material = Resources.Load("Materials/PlayerMatTransparent") as Material;        
        Color playerColor = mat.color;
        invisible = true;
        for(int i = 0; i < 7; i++) {
            playerColor.a -= 0.1f;
            mat.color = playerColor;
            yield return null;
        }
        yield return new WaitForSeconds(5);
        for (int i = 0; i < 7; i++) {
            playerColor.a += 0.1f;
            mat.color = playerColor;
            yield return null;
        }
        transform.Find("Engineer").GetComponent<SkinnedMeshRenderer>().material = Resources.Load("Materials/PlayerMat") as Material;
        invisible = false;
        yield return null;
    }

    public void FootStep() {
        if (fast) {
            audio.volume = 1;
        }
        else {
            audio.volume = 0.2f;
        }
        audio.Play();
    }

    private void OnTriggerEnter(Collider collider){
        if (collider.gameObject.CompareTag("Item")) {
            Events.EventsMgr.Instance.TriggerEvent(new Events.ItemCollected(collider.gameObject.GetComponent<Item>()));
            collider.GetComponent<Item>().Deactivate();
        }
    }

}
