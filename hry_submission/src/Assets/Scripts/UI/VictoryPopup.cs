﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryPopup : Popup
{
    [SerializeField]
    Button mainMenuButton;
    // Start is called before the first frame update
    void Start() {
        Cursor.visible = true;
        mainMenuButton.onClick.AddListener(ToMainMenu);
    }

    void ToMainMenu() {
        LogicMgr.Instance.ToMainMenuScene();
        Close();
    }
}
