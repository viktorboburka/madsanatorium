﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Events
{
    public class LevelProgress : EventType
    {

         //this is the variable that will hold the colliding object
         //If we only want to detect the first time it's triggered

        void Start()
        {

        }

        void OnTriggerEnter(Collider other)
        {
            /*if (other != GameObject.FindWithTag("Player")) //The colliding object isn't our object
            {
                return; //don't do anything if it's not our target
            }*/
            //print(other.tag);
            print(other.tag);
            print("triggered");
            if (other.tag != "Untagged") return;
            print("triggered\n");
            Application.LoadLevel("Level2");
                              //If you want to have this only trigger once
                              //Other code to do stuff on trigger goes here. You could:
                              // 1. Increase a variable to keep track of score
                              // 2. Transform the colliding object (send the player flying, etc)
                              // 3. Play a sound (audio.Play() if you have an audio source)
                              // 4. And much, much more
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}