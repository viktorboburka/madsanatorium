﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Events
{
    public class ItemCollected : EventType
    {
        public Item item;

        public ItemCollected() { }
        public ItemCollected(Item item) {
            this.item = item;
        }
        // Start is called before the first frame update
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }
    }
}
