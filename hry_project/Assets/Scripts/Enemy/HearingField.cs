﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HearingField : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private bool checkIfAbove(Transform pos)
    {
        if (GetComponentInParent<Enemy>().transform.position.y + 1 < pos.position.y)
        {
            return true;
        }
        return false;
    }

    private bool checkIfBelow(Transform pos)
    {
        if (GetComponentInParent<Enemy>().transform.position.y > pos.position.y + 1)
        {
            return true;
        }
        return false;
    }

    private bool checkIfSimilar(Transform pos)
    {
        if (GetComponentInParent<Enemy>().transform.position.y + 1 > pos.position.y && GetComponentInParent<Enemy>().transform.position.y < pos.position.y + 1)
        {
            return true;
        }
        return false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (checkIfSimilar(LogicMgr.Instance.GetPlayer().transform))
        {
            GetComponentInParent<EnemyAI>().ladder = null;
        }

        //prepare ladder that i could possibly use
        else if (other.name == "Ladder" &&  GetComponentInParent<EnemyAI>().gameObject.tag == "Zombie" && (checkIfSimilar(other.GetComponent<Ladder>().start) || (checkIfSimilar(other.GetComponent<Ladder>().end))))
        {
            //player is above us, look for ladders leading up
            if (checkIfAbove(LogicMgr.Instance.GetPlayer().transform) && checkIfSimilar(other.GetComponent<Ladder>().start))
            {
                if (GetComponentInParent<EnemyAI>().ladder == null)
                {
                    RaycastHit hit;
                    Ray raycast = new Ray(transform.position + GetComponentInParent<EnemyAI>().dir * 0.5f * Vector3.right, (other.GetComponent<Ladder>().start.position - (transform.position + GetComponentInParent<EnemyAI>().dir * 0.5f * Vector3.right)));
                    if (Physics.Raycast(raycast, out hit, 20))
                    {
                        if (hit.collider.gameObject.name == "Ladder")
                        {
                            //print("raycast hit");
                            GetComponentInParent<EnemyAI>().ladder = other.gameObject;
                        }
                    }
                }
                else if (Mathf.Abs(Vector3.Distance(GetComponentInParent<EnemyAI>().ladder.GetComponent<Ladder>().end.position, LogicMgr.Instance.GetPlayer().transform.position)) > Mathf.Abs(Vector3.Distance(other.gameObject.GetComponent<Ladder>().end.position, LogicMgr.Instance.GetPlayer().transform.position)))
                {

                    RaycastHit hit;
                    Ray raycast = new Ray(transform.position + GetComponentInParent<EnemyAI>().dir * 0.5f * Vector3.right, (other.GetComponent<Ladder>().start.position - (transform.position + GetComponentInParent<EnemyAI>().dir * 0.5f * Vector3.right)));
                    if (Physics.Raycast(raycast, out hit, 20))
                    {
                        if (hit.collider.gameObject == other.gameObject)
                        {
                            //print("raycast hit");
                            print(GetComponentInParent<EnemyAI>().ladder.GetComponent<Ladder>().end.position);
                            GetComponentInParent<EnemyAI>().ladder = other.gameObject;
                        }
                    }
                }
            }
            //player is below, look for ladders leading down
            else if (checkIfBelow(LogicMgr.Instance.GetPlayer().transform) && checkIfSimilar(other.GetComponent<Ladder>().end))
            {
                if (GetComponentInParent<EnemyAI>().ladder == null)
                {
                    RaycastHit hit;
                    Ray raycast = new Ray(transform.position + GetComponentInParent<EnemyAI>().dir * 0.5f * Vector3.right, (other.GetComponent<Ladder>().end.position - (transform.position + GetComponentInParent<EnemyAI>().dir * 0.5f * Vector3.right)));
                    if (Physics.Raycast(raycast, out hit, 20))
                    {
                        if (hit.collider.gameObject == other.gameObject)
                        {
                            //print("raycast hit");
                            GetComponentInParent<EnemyAI>().ladder = other.gameObject;
                        }
                    }
                }
                else if (Mathf.Abs(Vector3.Distance(GetComponentInParent<EnemyAI>().ladder.GetComponent<Ladder>().start.position, LogicMgr.Instance.GetPlayer().transform.position)) > Mathf.Abs(Vector3.Distance(other.GetComponent<Ladder>().start.position, LogicMgr.Instance.GetPlayer().transform.position)))
                {

                    RaycastHit hit;
                    Ray raycast = new Ray(transform.position + GetComponentInParent<EnemyAI>().dir * 0.5f * Vector3.right, (other.GetComponent<Ladder>().end.position - (transform.position + GetComponentInParent<EnemyAI>().dir * 0.5f * Vector3.right)));
                    if (Physics.Raycast(raycast, out hit, 20))
                    {
                        if (hit.collider.gameObject.name == "Ladder")
                        {
                            //print("raycast hit");
                            print(GetComponentInParent<EnemyAI>().ladder.GetComponent<Ladder>().end.position);
                            GetComponentInParent<EnemyAI>().ladder = other.gameObject;
                        }
                    }
                }
            }

        }

        //player entered hearing field
        if (other.tag == "Player" && !LogicMgr.Instance.GetPlayer().movement.getStealthed() &&
            GetComponentInParent<EnemyAI>().getState() != EnemyState.chasing && !LogicMgr.Instance.GetPlayer().GetComponent<PlayerMovement>().stationary && (LogicMgr.Instance.WorldOrientation() == GetComponentInParent<EnemyAI>().GetOrientation()))
        {
            if (GetComponentInParent<Enemy>().transform.position.y + 1.5 < other.gameObject.transform.position.y || GetComponentInParent<Enemy>().transform.position.y > other.gameObject.transform.position.y + 1.5)
            {
                if (GetComponentInParent<EnemyAI>().ladder != null)
                {
                    GetComponentInParent<EnemyAI>().Heard(GetComponentInParent<EnemyAI>().ladder.transform.position);
                    GetComponentInParent<EnemyAI>().goingToLadder = true;
                    return;
                }
                return;
            }

            GetComponentInParent<EnemyAI>().Heard(other.transform.position);
        }
    }
    

    private void OnTriggerExit(Collider other)
    {
        if(other.name == "Ladder" && other.gameObject == GetComponentInParent<EnemyAI>().ladder)
        {
            GetComponentInParent<EnemyAI>().ladder = null;
        }
    }
    
}
