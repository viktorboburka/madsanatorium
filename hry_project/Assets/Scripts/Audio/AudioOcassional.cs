﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioOcassional : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    AudioSource audio;

    [SerializeField]
    List<AudioClip> clips;
    void Start()
    {
        StartCoroutine(PlaySound());
    }
    //Plays occassional ambience sounds
    IEnumerator PlaySound() {
        while (true) {
            yield return new WaitForSeconds(Random.Range(15, 50));
            audio.clip = clips[Random.Range(0, clips.Count - 1)];
            audio.Play();
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
