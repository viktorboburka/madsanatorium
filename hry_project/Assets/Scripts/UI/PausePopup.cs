﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PausePopup : Popup
{
    [SerializeField]
    Button backButton;

    [SerializeField]
    Button retryButton;

    [SerializeField]
    Button controlsButton;

    [SerializeField]
    Button mainMenuButton;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
        backButton.onClick.AddListener(Resume);
        retryButton.onClick.AddListener(Retry);
        controlsButton.onClick.AddListener(Controls);
        mainMenuButton.onClick.AddListener(ToMainMenu);
    }

    void Resume() {
        Cursor.visible = false;
        Close();
    }
    void Retry() {
        Cursor.visible = false;
        LogicMgr.Instance.ReloadScene();
        Close();
    }

    void Controls() {
        Popups.CreateControlsInGamePopup(transform);
    }

    void ToMainMenu() {
        LogicMgr.Instance.ToMainMenuScene();
        Close();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
