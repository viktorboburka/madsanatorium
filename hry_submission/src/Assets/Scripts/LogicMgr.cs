﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogicMgr
{
    private static LogicMgr instance = null;
    public static LogicMgr Instance {
        get {
            if (instance == null) {
                instance = new LogicMgr();
                instance.Init();
                return instance;
            }
            else {
                return instance;
            }
        }
    }

    World world;

    Player player = null;

    bool isPaused;
    bool isRotating;
    bool isClimbing;
    bool isGameOver;
    void Reload(Scene scene, LoadSceneMode mode) {
        Init();
    }
    void Init() {
        isPaused = false;
        isRotating = false;
        isGameOver = false;
        SceneManager.sceneLoaded += Reload;

        if(SceneManager.GetActiveScene().name == "MainMenuScene") {
            return;
        }
        player = GameObject.Find("Player").GetComponent<Player>();
        world = GameObject.Find("World").GetComponent<World>();
    }
    public Player GetPlayer() {
        if(player == null) {
            player = GameObject.Find("Player").GetComponent<Player>();
        }
        return player;
    }
    public bool IsPaused() {
        return isPaused;
    }

    public bool IsRotating() {
        return isRotating;
    }

    public bool IsGameOver() {
        return isGameOver;
    }

    public World.Orientation WorldOrientation() {
        return world.WorldOrientation();
    } 
    
    //Function for door transition
    public void RotateWorld(Door door) {
        isRotating = true;
        if(WorldOrientation() == World.Orientation.side) {
            //hides/shows front/back corridors when in side corridor
            Camera.main.cullingMask |= 1 << LayerMask.NameToLayer("NotSide");
        }
        world.StartCoroutine(world.RotateWorld(door, () => { 
            isRotating = false; 
            if(WorldOrientation() == World.Orientation.side) {
                //hides/shows front/back corridors when in side corridor
                Camera.main.cullingMask &= ~(1 << LayerMask.NameToLayer("NotSide"));
            }
        }));
    }

    public void GameOver() {
        isGameOver = true;
        GetPlayer().Death(() => {
            Popup popup = Popups.CreateGameOverPopup(GameObject.Find("UI").GetComponent<GameUI>().transform);
            PauseGame();
            popup.onClose += () => { UnPauseGame(); };
        });        
    }

    public void Victory() {
        isGameOver = true;
        Popup popup = Popups.CreateVictoryPopup(GameObject.Find("UI").GetComponent<GameUI>().transform);
        PauseGame();
        popup.onClose += () => { UnPauseGame(); };
    }

    public void ReloadScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Init();
    }

    public void ToMainMenuScene() {
        SceneManager.LoadScene("MainMenuScene");
        Init();
    }

    public void Pause() {
        Popup popup = Popups.CreatePausePopup(GameObject.Find("UI").GetComponent<GameUI>().transform);
        PauseGame();
        popup.onClose += () => { UnPauseGame(); };
    }

    void UnPauseGame() {
        isPaused = false;
        Time.timeScale = 1.0f;
    }
    void PauseGame() {
        isPaused = true;
        Time.timeScale = 0.0f;
    }    
}
