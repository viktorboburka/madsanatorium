﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationTransitions : MonoBehaviour
{
    Animator animator;
    EnemyAI enemyAI;
    bool isChasing;
    EnemyState enemyState;
    //int c = 0;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        enemyAI = GetComponent<EnemyAI>();
    }

    // Update is called once per frame
    void Update()
    {
        //c++;
        //if (c%60 == 0)print("updating anim");
        enemyState = enemyAI.getState();
        if (enemyState == EnemyState.chasing)
        {
            isChasing = true;
            //if (c % 60 == 0) print("is chasing");
        }
        else
        {
            isChasing = false;
            //if (c % 60 == 0) print("isnt chasing");
        }
        if(enemyAI.gameObject.tag == "Zombie") {
            //print(enemyAI.GetSpeed());
        }
        animator.SetFloat("speed", enemyAI.GetSpeed());
        animator.SetBool("isChasing", isChasing);
    }
}
