﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    [SerializeField]
    AudioSource audio;

    bool soundEmitted = false;

    float soundStrength = 10.0f;
    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    IEnumerator Disappear() {
        yield return new WaitForSeconds(4);
        Destroy(gameObject);
    }

    //This happens when rock hits somethings - triggers sound event
    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Player") {
            Physics.IgnoreCollision(collision.collider, gameObject.GetComponent<MeshCollider>());
        } else {
            audio.Play();
            if (!soundEmitted && collision.gameObject.tag != "Enemy") {
                Events.EventsMgr.Instance.TriggerEvent(new Events.SoundEmitted(transform.position, soundStrength));
                StartCoroutine(Disappear());
                soundEmitted = true;
            }
            
        }
    }
}
