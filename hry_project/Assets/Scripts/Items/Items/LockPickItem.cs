﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockPickItem : Item
{
    //Unlocks door
    public override bool Use() {

        Interactable i = LogicMgr.Instance.GetPlayer().controls.GetInteractable();
        if (i != null && i is Door) {
            if (((Door)i).isLocked) {
                GetComponent<ItemAudio>().playSound();
                ((Door)i).Unlock();
                return true;
            }
        }
        return false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
