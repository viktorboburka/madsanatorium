﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverPopup : Popup
{
    [SerializeField]
    Button retryButton;

    [SerializeField]
    Button mainMenuButton;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
        retryButton.onClick.AddListener(Retry);
        mainMenuButton.onClick.AddListener(ToMainMenu);
    }

    void Retry() {
        Cursor.visible = false;
        LogicMgr.Instance.ReloadScene();
        Close();
    }

    void ToMainMenu() {
        LogicMgr.Instance.ToMainMenuScene();
        Close();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
