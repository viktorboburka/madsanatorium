﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour
{
    [SerializeField]
    Button startButton;

    [SerializeField]
    Button levelsButton;

    [SerializeField]
    Button controlsButton;

    [SerializeField]
    Button exitButton;

    string maxLevel;

    string maxLevelPath;
        
    void InitButtons() {
        startButton.onClick.AddListener(StartNewGame);
        levelsButton.onClick.AddListener(OpenLevelsMenu);
        controlsButton.onClick.AddListener(OpenControlsMenu);
        exitButton.onClick.AddListener(ExitGame);
    }


    //Initialize main menu
    void Start()
    {
        InitButtons();
        maxLevelPath = Application.persistentDataPath + "/MaxLevel.txt";
        if(!File.Exists(maxLevelPath)) {
            File.WriteAllText(maxLevelPath, "1");
        }
        maxLevel = File.ReadAllText(maxLevelPath);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ExitGame() {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }
    void OpenLevelsMenu() {
        LevelsPopup p = Popups.CreateLevelsPopup(transform) as LevelsPopup;
        p.ShowLevels(int.Parse(maxLevel));
    }

    void OpenControlsMenu() {
        ControlsPopup p = Popups.CreateControlsPopup(transform) as ControlsPopup;
    }

    void StartNewGame() {
        Cursor.visible = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
