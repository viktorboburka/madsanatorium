﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class World : MonoBehaviour
{

    public static Orientation GetNextOrientation(Door.Orientation doorOrientation, Orientation orientation) {
        if (doorOrientation == Door.Orientation.left) {
            if (orientation != Orientation.front) {
                orientation--;
            }
        } else {
            if (orientation != Orientation.back) {
                orientation++;
            }
        }
        return orientation;
    }
    public enum Orientation
    {
        front,
        side,
        back
    }

    float rotationSpeed = 100.0f;

    Orientation currentOrientation;

    //World rotation - world gets rotated, player moves through door, main orientation is set
    public IEnumerator RotateWorld(Door door, UnityAction completion) {
        ChangeOrientation(door);

        Transform player = LogicMgr.Instance.GetPlayer().transform;
        player.parent = transform;
        float playerY = player.eulerAngles.y;
        door.targetPlace.position = new Vector3(door.targetPlace.position.x, player.position.y, door.targetPlace.position.z);        
        player.GetComponent<Rigidbody>().isKinematic = true;
        if (door.orientation == Door.Orientation.right) {
            Vector3 targetRotation = transform.eulerAngles + new Vector3(0, 90, 0);
            while (transform.eulerAngles != targetRotation) {
                transform.RotateAround(door.transform.position, Vector3.up, Time.deltaTime * rotationSpeed);
                player.eulerAngles = new Vector3(player.eulerAngles.x, playerY, player.eulerAngles.z);
                player.position = Vector3.Lerp(player.position, door.targetPlace.position, Time.deltaTime * rotationSpeed / 40);

                if (transform.eulerAngles.y > targetRotation.y) {
                    transform.eulerAngles = targetRotation;
                }
                yield return null;
            }
        } else {
            Vector3 targetRotation = transform.eulerAngles - new Vector3(0, 90, 0);
            while (transform.eulerAngles != targetRotation) {
                transform.RotateAround(door.transform.position, Vector3.up, -Time.deltaTime * rotationSpeed);
                player.eulerAngles = new Vector3(player.eulerAngles.x, playerY, player.eulerAngles.z);
                player.position = Vector3.Lerp(player.position, door.targetPlace.position, Time.deltaTime * rotationSpeed / 40);                
                if (transform.eulerAngles.y < targetRotation.y || transform.eulerAngles.y > 180) {
                    transform.eulerAngles = targetRotation;
                }
                yield return null;
            }
        }

        player.position = door.targetPlace.position;
        player.parent = transform.parent;
        player.GetComponent<Rigidbody>().isKinematic = false;
        completion();
        yield return null;
    }    

    public Orientation WorldOrientation() {
        return currentOrientation;
    }

    private void ChangeOrientation(Door door) {
        currentOrientation = GetNextOrientation(door.orientation, currentOrientation);
    }

    void Start()
    {
        currentOrientation = Orientation.front;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
