﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    float mult;


    // Start is called before the first frame update
    void Start()
    {
        if (GetComponentInParent<EnemyAI>().gameObject.tag == "Enemy")
        {
            mult = 2.8f;
        }
        else if (GetComponentInParent<EnemyAI>().gameObject.tag == "Zombie")
        {
            mult = 2.7f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        //player entered our field of view, if raycasts didnt encounter any obstacle, start chasing him
        if (other.tag == "Player" && !LogicMgr.Instance.GetPlayer().movement.getInvisible() && (GetComponentInParent<EnemyAI>().GetOrientation() == LogicMgr.Instance.WorldOrientation()))
        {
            print("player spotted, my pos " + GetComponentInParent<EnemyAI>().gameObject.transform.position.x + " player " + LogicMgr.Instance.GetPlayer().transform.position.x);
            RaycastHit hit;
            Ray[] rays = new Ray[3];
            for (int i = -1; i < 1; i++)
            {

                rays[i+1] = new Ray(transform.position - (GetComponentInParent<EnemyAI>().dir * mult * Vector3.right), (other.transform.position  + ((i+1.5f) * Vector3.up)) - (transform.position - (GetComponentInParent<EnemyAI>().dir * mult * Vector3.right)));
               
                gameObject.GetComponent<MeshCollider>().enabled = false;
                Debug.DrawRay(transform.position - (GetComponentInParent<EnemyAI>().dir * mult * Vector3.right), (other.transform.position + ((i + 1.5f) * Vector3.up)) - (transform.position - (GetComponentInParent<EnemyAI>().dir * mult * Vector3.right)));
                if (Physics.Raycast(rays[i + 1], out hit, Mathf.Infinity))
                {
                    if (hit.collider.gameObject.CompareTag("Player"))
                    {
                        GetComponentInParent<EnemyAI>().Chase();
                        //print("chasing, my pos " + GetComponentInParent<EnemyAI>().gameObject.transform.position.x + " player " + LogicMgr.Instance.GetPlayer().transform.position.x);
                        break;
                    }
                }
            }

            gameObject.GetComponent<MeshCollider>().enabled = true;
        }
    }

    


}
