﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(EnemyAI))]
public class Enemy : MonoBehaviour
{
    AudioSource audio;
    public EnemyAI ai { get; private set; }

    public FieldOfView f { get; private set; }
    
    public HearingField h { get; private set; }

    public StealthHearingField sh { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        ai = GetComponent<EnemyAI>();
        f = GetComponent<FieldOfView>();
        h = GetComponent<HearingField>();
        sh = GetComponent<StealthHearingField>();
        audio = transform.Find("Footstep").gameObject.GetComponent<AudioSource>();
    }

    public void FootStep() {
        if(ai.aiState == EnemyState.chasing) {
            audio.volume = 0.7f;
        }
        else {
            audio.volume = 0.3f;
        }
        audio.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
