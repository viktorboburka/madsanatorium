﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Interactable
{
    [SerializeField]
    Door pairDoor;
    public enum Orientation
    {
        left,
        right
    }

    public Orientation orientation;
    public Transform targetPlace;
    public bool isLocked;

    protected override void Highlight() {
        if (isLocked) {
            GetComponent<MeshRenderer>().material.color = Color.red;
        } else {
            GetComponent<MeshRenderer>().material.color = Color.green;
        }
    }

    protected override void UnHighlight() {
        GetComponent<MeshRenderer>().material.color = Color.white;
    }

    public override void Interact() {
        //Begins transition
        if (!LogicMgr.Instance.IsRotating() && !isLocked) {
            gameObject.GetComponent<ItemAudio>().playSound();
            Events.EventsMgr.Instance.TriggerEvent(new Events.SoundEmitted(transform.position, 4.0f));
            LogicMgr.Instance.RotateWorld(this);
        }
    }

    public void UnlockPair() {
        isLocked = false;
    }

    public void Unlock() {
        isLocked = false;
        pairDoor.UnlockPair();
        Highlight();
        //change texture here
    }

    //Special trigger check for doors
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            //Debug.Log("Highlighting " + this.gameObject.tag);
            Highlight();
            other.GetComponent<Player>().controls.AddInteraction(this);
        }
        if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Zombie") {
            other.GetComponent<EnemyAI>().DoorCollider(this);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
