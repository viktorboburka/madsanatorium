﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightItem : Item
{
    [SerializeField]
    GameObject simpleLight;

    //Places static light
    public override bool Use() {
        GetComponent<ItemAudio>().playSound();
        Transform player = LogicMgr.Instance.GetPlayer().transform;
        Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        clickPosition.Set(clickPosition.x, clickPosition.y, player.position.z);
        Vector3 direction = (clickPosition - player.position).normalized;
        GetComponent<ItemAudio>().playSound();
        
        if(Vector3.Distance(clickPosition, player.position) > 2) {
            clickPosition = player.position + direction * 2 + Vector3.up / 2;
        }


        Instantiate(simpleLight, clickPosition, Quaternion.identity, GameObject.Find("World").transform);
        return true;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
