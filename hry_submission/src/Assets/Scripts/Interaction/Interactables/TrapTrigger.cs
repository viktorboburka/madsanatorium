﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapTrigger : Interactable
{
    Player player;
    // Start is called before the first frame update
    void Start()
    {
        player = LogicMgr.Instance.GetPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    protected override void Highlight()
    {
        //Debug.Log("TriggerTrap Highlighted\n");
        Interact();
    }

    protected override void UnHighlight()
    {
        //Debug.Log("TriggerTrap UnHighlighted\n");
    }

    //Death trigger
    public override void Interact()
    {
        Debug.Log("Triggered trap\n");
        gameObject.GetComponent<ItemAudio>().playSound();
        LogicMgr.Instance.GameOver();
    }
}
