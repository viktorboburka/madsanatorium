﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class FlickeringLight : MonoBehaviour
{
    Light flickerLight;

    [SerializeField]
    float maxIntensity = 1.0f;

    [SerializeField]
    float minIntensity = 0.25f;

    [Range(2f, 10f)]
    [SerializeField]
    float frequency = 2.0f;

    [SerializeField]
    float step = 0.6f;

    [SerializeField]
    bool inverted = false;

    [SerializeField]
    bool audioOn = false;

    AudioSource audio = null;

    // Start is called before the first frame update
    void Start()
    {
        flickerLight = GetComponent<Light>();
        flickerLight.intensity = minIntensity;
        if (audioOn) {
            audio = gameObject.AddComponent<AudioSource>();
            audio.volume = 0.3f;
            audio.spatialBlend = 1;
            audio.minDistance = 3;
            audio.clip = Resources.Load("Sounds/Zap") as AudioClip;
        }
        StartCoroutine(Flickering());
    }

    //Makes light flicker, can be inverted
    IEnumerator Flickering() {
        if (!inverted) {
            while (true) {
                yield return new WaitForSeconds(Random.Range(1, frequency));                
                while (flickerLight.intensity < maxIntensity) {
                    flickerLight.intensity += step;
                    flickerLight.intensity = Mathf.Min(flickerLight.intensity, maxIntensity);
                    yield return null;
                }
                if (audioOn) {
                    audio.Play();
                }
                while (flickerLight.intensity > minIntensity) {
                    flickerLight.intensity -= step;
                    flickerLight.intensity = Mathf.Max(flickerLight.intensity, minIntensity);
                    yield return null;
                }
            }
        }
        else {
            while (true) {
                flickerLight.intensity = maxIntensity;
                yield return new WaitForSeconds(Random.Range(1, frequency));
                if (audioOn) {
                    audio.Play();
                }
                while (flickerLight.intensity > minIntensity) {
                    flickerLight.intensity -= step;
                    flickerLight.intensity = Mathf.Max(flickerLight.intensity, minIntensity);
                    yield return null;
                }
                while (flickerLight.intensity < maxIntensity) {
                    flickerLight.intensity += step;
                    flickerLight.intensity = Mathf.Min(flickerLight.intensity, maxIntensity);
                    yield return null;
                }                
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
