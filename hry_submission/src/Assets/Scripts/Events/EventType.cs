﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Events
{
    public class EventType : MonoBehaviour
    {
        public readonly string eventId;
        public EventType() { this.eventId = this.GetType().FullName; }

        // Start is called before the first frame update
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }
    }
}
