﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTransitions : MonoBehaviour
{

    Animator animator;
    PlayerMovement pm;
    float xSpeed;
    bool isSneaking;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        pm = GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        xSpeed = pm.getxSpeed();
        //isSneaking = pm.getStealthed();
        if (Input.GetKey(KeyCode.LeftShift))
        {
            isSneaking = true;
        }
        else
        {
            isSneaking = false;
        }

        if(Mathf.Sign(xSpeed) != Mathf.Sign(LogicMgr.Instance.GetPlayer().flashlight.direction)) {
            animator.SetFloat("inverse", -1 );
        }
        else {
            animator.SetFloat("inverse", 1);
        }
        
        animator.SetFloat("xSpeed", Mathf.Abs(xSpeed * 25));
        animator.SetBool("isSneaking", isSneaking);
    }
}
