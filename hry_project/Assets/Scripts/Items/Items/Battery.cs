﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : Item
{
    public override bool Use() {

        GetComponent<ItemAudio>().playSound();
        LogicMgr.Instance.GetPlayer().flashlight.FillBattery();
        return true;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
