﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public abstract class Interactable : MonoBehaviour
{
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected abstract void Highlight();
    protected abstract void UnHighlight();
    public abstract void Interact();

    //This happens when player gets close to interactable
    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "Player") {
            //Debug.Log("Highlighting " + this.gameObject.tag);
            Highlight();
            other.GetComponent<Player>().controls.AddInteraction(this);
        }
    }

    //Removes interaction when far from interactable
    private void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Player") {
            UnHighlight();
            other.GetComponent<Player>().controls.RemoveInteraction();
        }
    }
}
