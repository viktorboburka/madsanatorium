﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : Interactable {
    [SerializeField]
    public Transform start;

    [SerializeField]
    public Transform end;

    float moveSpeed = 0.4f;
    bool isClimbing = false;

    //Climbing method for player
    IEnumerator ClimbLadder() {
        isClimbing = true;
        Transform player = LogicMgr.Instance.GetPlayer().transform;
        float lastRotation = player.eulerAngles.y;
        player.rotation = Quaternion.Euler(0, 0, 0);
        player.GetComponent<Animator>().Play("Climbing");
        Events.EventsMgr.Instance.TriggerEvent(new Events.SoundEmitted(transform.position, 4.0f));
        LogicMgr.Instance.GetPlayer().movement.SetClimbing(isClimbing);
        player.GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionY;
        Vector3 playerPos = player.position;
        Vector3 targetPosition = Vector3.Distance(playerPos, start.position) < Vector3.Distance(playerPos, end.position) ? end.position : start.position;
        targetPosition = new Vector3(targetPosition.x, targetPosition.y, playerPos.z);
        Vector3 offset = Vector3.Lerp(player.position, targetPosition, Time.deltaTime * moveSpeed) - player.position;
        while (Mathf.Abs(player.transform.position.y - targetPosition.y) > 0.1) {
            while (LogicMgr.Instance.IsPaused()) {
                yield return null;
            }
            player.transform.position += offset;
            yield return null;
        }
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        if (LogicMgr.Instance.GetPlayer().movement.getStealthed()) {
            player.GetComponent<Animator>().Play("Sneak");
        }
        else {
            player.GetComponent<Animator>().Play("Idle");
        }
        isClimbing = false;
        player.rotation = Quaternion.Euler(0, lastRotation, 0);
        LogicMgr.Instance.GetPlayer().movement.SetClimbing(isClimbing);
        LogicMgr.Instance.GetPlayer().lastLadderUsed = gameObject;
        yield return null;
    }

    //Climbing method for enemy
    IEnumerator EnemyClimbLadder(GameObject other) {
        EnemyAI ai = other.GetComponent<EnemyAI>();
        ai.setClimbing(true);
        float initDir = other.GetComponent<EnemyAI>().dir;
        other.GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionY;
        float lastRotation = other.transform.eulerAngles.y;
        other.transform.rotation = Quaternion.Euler(0, 0, 0);
        World.Orientation orient = LogicMgr.Instance.WorldOrientation();
        other.transform.GetComponent<Animator>().Play("Climbing");
        Vector3 enemyPos = other.transform.position;
        print(enemyPos.x);
        Vector3 targetPosition = Vector3.Distance(enemyPos, start.position) < Vector3.Distance(enemyPos, end.position) ? end.position : start.position;
        targetPosition = new Vector3(targetPosition.x, targetPosition.y, enemyPos.z);

        Vector3 offset = Vector3.Lerp(other.transform.position, targetPosition, Time.deltaTime * moveSpeed) - other.transform.position;
        while (Mathf.Abs(other.transform.position.y - targetPosition.y) > 0.1) {
            while (LogicMgr.Instance.IsPaused()) {
                yield return null;
            }
            other.transform.position += offset;
            yield return null;
        }
        if (initDir != other.GetComponent<EnemyAI>().dir) lastRotation *= -1;
        other.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        ai.setClimbing(false);
        ai.UpdateState(EnemyState.patrolling);
        ai.goingToLadder = false;
        ai.ladder = null;
        if (LogicMgr.Instance.WorldOrientation() == orient) {
            other.transform.rotation = Quaternion.Euler(0, lastRotation, 0);
        }
        else {
            if (LogicMgr.Instance.WorldOrientation() == World.Orientation.front && ai.GetOrientation() == World.Orientation.side ||
                LogicMgr.Instance.WorldOrientation() == World.Orientation.side && ai.GetOrientation() == World.Orientation.back) {
                other.transform.rotation = Quaternion.Euler(0, lastRotation - 90, 0);
            }
            else {
                other.transform.rotation = Quaternion.Euler(0, lastRotation + 90, 0);
            }
        }
        other.GetComponent<Animator>().Play("Walk");
        yield return null;
    }


    public override void Interact() {
        if (!isClimbing) {
            StartCoroutine(ClimbLadder());
        }
    }

    public void EnemyInteract(GameObject other) {
        StartCoroutine(EnemyClimbLadder(other));
    }

    public void StopClimbing() {
        StopAllCoroutines();
    }


    protected override void Highlight() {
        GetComponent<MeshRenderer>().material.color = Color.green;
    }

    protected override void UnHighlight() {
        GetComponent<MeshRenderer>().material.color = Color.white;
    }

    //Special trigger check for ladder
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            Highlight();
            other.GetComponent<Player>().controls.AddInteraction(this);
        }

        if (other.gameObject.tag == "Zombie") {            
                other.gameObject.GetComponent<EnemyAI>().CloseToLadder(true, gameObject);
        }

    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Zombie") {
            other.gameObject.GetComponent<EnemyAI>().CloseToLadder(false, gameObject);
        }
        if (other.gameObject.tag == "Player") {
            UnHighlight();
            other.GetComponent<Player>().controls.RemoveInteraction();
        }
    }
}
