﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAudio : MonoBehaviour
{
    AudioSource audio;
    [SerializeField]
    AudioClip investigateSound;
    [SerializeField]
    AudioClip chaseSound;
    [SerializeField]
    List<AudioClip> ambienceSounds;
    public void PlayInvestigateSound() {
        audio.clip = investigateSound;
        audio.Play();
    }

    public void PlayChaseSound() {
        audio.clip = chaseSound;
        audio.Play();
    }

    private void Start() {
        audio = GetComponent<AudioSource>();
    }
}
