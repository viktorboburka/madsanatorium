# Skupinový projekt v rámci předmětu Počítačové hry na FEL ČVUT

## Členové týmu

* Viktor Bobůrka (Team leader) <boburvik@fel.cvut.cz>
* Michal Zajíc <zajicm13@fel.cvut.cz>
* Adam Pešek <pesekad1@fel.cvut.cz>
* Tomáš Pokorný <pokort20@fel.cvut.cz>
