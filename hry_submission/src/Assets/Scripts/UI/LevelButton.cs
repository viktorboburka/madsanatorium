﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    [SerializeField]
    Text label;
    [SerializeField]
    Image image;
    Button button;
    // Start is called before the first frame update
    void Start()
    {        
    }

    //Button is placed in levels popup
    public void Init(JSONObject level, int buildIndex) {
        label.text = level["name"].str;
        Sprite sprite = Resources.Load<Sprite>("Sprites/LevelSprites/" + level["imageName"].str);
        image.sprite = sprite;
        print("Sprites/LevelSprites/" + level["imageName"].str);
        button = GetComponent<Button>();
        button.onClick.AddListener(() => {
            Cursor.visible = false;
            SceneManager.LoadScene(buildIndex); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
