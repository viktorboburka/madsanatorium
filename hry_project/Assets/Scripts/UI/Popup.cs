﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup : MonoBehaviour
{
    public delegate void OnClose();
    public event OnClose onClose;
    // Start is called before the first frame update
    void Start()
    {
    }

    //What happens when popup is closed
    protected void Close() {        
        onClose?.Invoke();
        Destroy(gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
