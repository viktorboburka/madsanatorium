﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : Interactable
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void Highlight()
    {
        Debug.Log("Trap highlighted\n");
        GetComponent<MeshRenderer>().material.color = Color.green;
    }

    protected override void UnHighlight()
    {
        Debug.Log("Trap unhighlighted\n");
        GetComponent<MeshRenderer>().material.color = Color.white;
    }

    //Disarm interaction
    public override void Interact()
    {
        Debug.Log("Started interacting with trap\n");
        GetComponent<ItemAudio>().playSound();
        gameObject.SetActive(false);
    }
}
