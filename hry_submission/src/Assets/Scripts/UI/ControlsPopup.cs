﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlsPopup : Popup
{
    [SerializeField]
    Button close;

    void Start() {
        close.onClick.AddListener(Controls);
    }

    void Controls() {
        Close();
    }
}
