﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    public class EventsMgr
    {
        private static EventsMgr instance = null;
        public static EventsMgr Instance {
            get {
                if (instance == null) {
                    instance = new EventsMgr();
                    instance.Init();
                    return instance;
                }
                else {
                    return instance;
                }
            }
        }

        class Event : UnityEvent<EventType> { };
        private Dictionary<string, Event> eventDictionary;

        void Init() {
            if (eventDictionary == null) {
                eventDictionary = new Dictionary<string, Event>();
            }
        }

        //Adds event
        public void StartListening(EventType eventType, UnityAction<EventType> listener) {
            StartListening(eventType.eventId, listener);
        }
        public void StartListening (string eventName, UnityAction<EventType> listener){
        Event thisEvent = null;
        if (instance.eventDictionary.TryGetValue (eventName, out thisEvent))
        {
            thisEvent.AddListener (listener);
        } 
        else
        {
            thisEvent = new Event();
            thisEvent.AddListener (listener);
            instance.eventDictionary.Add (eventName, thisEvent);
        }
    }

        public void StopListening(EventType eventType, UnityAction<EventType> listener) {
            StopListening(eventType.eventId, listener);
        }
        public void StopListening (string eventName, UnityAction<EventType> listener)
        {
            if (instance == null) return;
            Event thisEvent = null;
            if (instance.eventDictionary.TryGetValue (eventName, out thisEvent))
            {
                thisEvent.RemoveListener (listener);
            }
        }



        public void TriggerEvent(EventType eventType) {
            Event thisEvent = null;
            if (instance.eventDictionary.TryGetValue(eventType.eventId, out thisEvent)) {
                thisEvent.Invoke(eventType);
            }
        }

    }
}
