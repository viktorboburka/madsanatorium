{
  "title": "MadSanatorium",
  "team": 8,
  "authors": [
    {"fullname": "Michal Zajíc", "email": "zajicm13@fel.cvut.cz"},
    {"fullname": "Adam Pešek", "email": "pesekad1@fit.cvut.cz"},
	{"fullname": "Tomáš Pokorný", "email": "pokort20@fel.cvut.cz"},
	{"fullname": "Viktor Bobůrka", "email": "boburvik@fel.cvut.cz"}
  ],
  "term": 191,
  "summary": "Horror stealth game, where you need to escape mad sanatorium full of dangers.",
  "opensource": true
}