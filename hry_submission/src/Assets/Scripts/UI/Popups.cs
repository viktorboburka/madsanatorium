﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popups : MonoBehaviour
{
    //This class creates all popups
    public static Popup CreateGameOverPopup(Transform parent) {
        GameObject popup = Instantiate(Resources.Load("Prefabs/UI/GameOverPopup") as GameObject, parent);
        Popup p = popup.GetComponent<GameOverPopup>();
        return p;
    }

    public static Popup CreatePausePopup(Transform parent) {
        GameObject popup = Instantiate(Resources.Load("Prefabs/UI/PausePopup") as GameObject, parent);
        Popup p = popup.GetComponent<PausePopup>();
        return p;
    }

    public static Popup CreateVictoryPopup(Transform parent) {
        GameObject popup = Instantiate(Resources.Load("Prefabs/UI/VictoryPopup") as GameObject, parent);
        Popup p = popup.GetComponent<VictoryPopup>();
        return p;
    }

    public static Popup CreateControlsPopup(Transform parent) {
        GameObject popup = Instantiate(Resources.Load("Prefabs/UI/ControlsPopup") as GameObject, parent);
        Popup p = popup.GetComponent<ControlsPopup>();
        return p;
    }

    public static Popup CreateControlsInGamePopup(Transform parent) {
        GameObject popup = Instantiate(Resources.Load("Prefabs/UI/ControlsInGamePopup") as GameObject, parent);
        Popup p = popup.GetComponent<ControlsPopup>();
        return p;
    }

    public static Popup CreateLevelsPopup(Transform parent) {
        GameObject popup = Instantiate(Resources.Load("Prefabs/UI/LevelsPopup") as GameObject, parent);
        Popup p = popup.GetComponent<LevelsPopup>();
        return p;
    }
}
