﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Events
{
    public class SoundEmitted : EventType
    {
        public Vector3 position;
        public float radius;

        public SoundEmitted() { }
        public SoundEmitted(Vector3 position, float radius) {
            this.position = position;
            this.radius = radius;
        }
    }
}
