﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFlashlight : MonoBehaviour
{
    //Flashlight transform
    [SerializeField]
    Transform flashlight;

    //Light itself
    [SerializeField]
    Light light;

    //Flashlight will lose power over time
    [SerializeField]
    float drainSpeed = 0.3f;

    //Light follows player head
    [SerializeField]
    Transform head;

    bool isOn;
    bool locked;
    float maxIntensity;
    public int direction {get;private set;}
    // Start is called before the first frame update
    void Start()
    {
        print(Application.persistentDataPath);
        isOn = true;
        locked = false;
        maxIntensity = light.intensity;
    }

    public void ToggleLock() {
        locked = !locked;
    }

    public void Switch() {
        if (isOn) {
            TurnOff();
        }
        else {
            TurnOn();
        }
    }
    void TurnOff() {
        flashlight.GetChild(0).gameObject.SetActive(false);
        isOn = false;
    }

    void TurnOn() {
        flashlight.GetChild(0).gameObject.SetActive(true);
        isOn = true;
    }

    public void FillBattery() {
        light.intensity = maxIntensity;
    }

    public Vector3 GetFlashlightOrientation() {
        return -flashlight.right;
    }
    public Vector3 GetFlashlightPosition() {
        return flashlight.position;
    }

    //Rotates flashlight and player based on mouse position
    void Update() {
        if (locked || LogicMgr.Instance.IsPaused() || LogicMgr.Instance.GetPlayer().movement.isClimbing) {            
            return;
        }
        if (LogicMgr.Instance.IsGameOver()) {
            flashlight.transform.position = new Vector3(head.position.x, head.position.y, flashlight.transform.position.z);
        }

        if (isOn) {
            light.intensity -= Time.deltaTime * drainSpeed;
        }
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(flashlight.position);
        Vector2 playerPosOnScreen = Camera.main.WorldToViewportPoint(transform.position);
        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);
        float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);        
        flashlight.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));

        if (playerPosOnScreen.x < mouseOnScreen.x) {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, 90, transform.eulerAngles.z);
            light.transform.localPosition = new Vector3(0, 0, 0);
        }
        else {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, -90, transform.eulerAngles.z);
            light.transform.localPosition = new Vector3(0.0f, 0, 0.9f);
        }
        if (direction == 1) {
            head.localRotation = Quaternion.Euler(new Vector3(0f, 0, angle + 180));
        }
        else {
            head.localRotation = Quaternion.Euler(new Vector3(0f, 0, -angle  ));
        }

        flashlight.transform.position = new Vector3(head.position.x, head.position.y, flashlight.transform.position.z);

        if(angle > -90 && angle < 90) {
            direction = -1;
        }
        else {
            direction = 1;
        }
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b) {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }
}
