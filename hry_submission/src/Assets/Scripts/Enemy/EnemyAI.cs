﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyState
{
    patrolling,
    chasing,
    investigating
}
public class EnemyAI : MonoBehaviour
{
    Player player;

    [SerializeField]
    public GameObject f;

    [SerializeField]
    GameObject h;

    [SerializeField]
    GameObject sh;

    EnemyAudio audio;

    public GameObject ladder;
    GameObject closestLadder;

    Transform p;
    public float dir = 1;
    public float speedMultiplier = 0.02f;
    float timeToTurn = 5.0f;
    public bool goingToLadder = false;
    bool isClimbing = false;
    bool isKiller = false;
    bool attacked = false;

    bool closeToLadder = false;

    FieldOfView fo;
    HearingField hf;
    StealthHearingField shf;

    float stateTime = 3.0f;

    Vector3 lastHeardPosition;

    bool locked = false;
    int doorTransitCooldown = 0;

    [SerializeField]
    World.Orientation currentOrientation;

    int activeAxis = 0;
    int leftRight = 1;

    public EnemyState aiState = EnemyState.patrolling;

    // Start is called before the first frame update
    void Start()
    {
	    player = LogicMgr.Instance.GetPlayer();
        p = player.transform;  
        fo = f.GetComponent<FieldOfView>();
        hf = h.GetComponent<HearingField>();
        shf = sh.GetComponent<StealthHearingField>();
        audio = GetComponent<EnemyAudio>();

        if (tag == "Zombie") {
            speedMultiplier /= 2.0f;
        }

        Events.EventsMgr.Instance.StartListening(new Events.SoundEmitted(), SoundEmitted);

        
    }

    // Update is called once per frame
    void Update()
    {
        if (LogicMgr.Instance.IsGameOver()) {
            GetComponent<Rigidbody>().isKinematic = true;
            if (isKiller) {
                if(!attacked)
                    GetComponent<Animator>().Play("Attack");
                attacked = true;
            }
            else {
                GetComponent<Animator>().Play("Idle");
            }
            return;
        }
        if (LogicMgr.Instance.IsPaused() || LogicMgr.Instance.IsRotating() || locked || isClimbing) {
            return;
        }


        //Update state here based on conditions
        //for example, if lost sight, set to investigating
        //or if investigating for 5 seconds, set to patrolling

        //UpdateState();

        //based on active state, update enemy speed, timers etc.
        if (aiState == EnemyState.chasing) {
            ChasePlayer();
        }
        else if(aiState == EnemyState.investigating) {
            Investigate();
        }
        else if(aiState == EnemyState.patrolling) {
            Patrol();
        }

        //Prepares active axis and +- multiplicator and finally moves the enemy
        SetMoveDirection();
        Vector3 move = new Vector3(0, 0, 0);
        //if (tag != "Zombie") {
            move[activeAxis] += leftRight * dir * speedMultiplier;
            transform.position += move;
        //}
    }

    public EnemyState getState()
    {
        return aiState;
    }

    public void setClimbing(bool value)
    {
        isClimbing = value;
    }

    public World.Orientation GetOrientation()
    {
        return currentOrientation;
    }

    public void UpdateState(EnemyState state) {
        stateTime = 3.0f;
        if(aiState == state) {
            return;
        }
        aiState = state;

        switch (state)
        {
            case EnemyState.chasing:
                speedMultiplier = 0.05f;
                if (tag == "Zombie") {
                    speedMultiplier /= 2.0f;
                }
                audio.PlayChaseSound();
                break;

            case EnemyState.investigating:
                speedMultiplier = 0.03f;
                if (tag == "Zombie") {
                    speedMultiplier /= 2.0f;
                }
                audio.PlayInvestigateSound();
                break;

            case EnemyState.patrolling:
                speedMultiplier = 0.02f;
                if (tag == "Zombie") {
                    speedMultiplier /= 2.0f;
                }
                timeToTurn = Random.Range(0.0f, 3.0f);
                goingToLadder = false;
                break;

            default:
                break;
        }


    }

    //state for when enemy hears something
    private void Investigate() {
        if (!isClimbing && closeToLadder && (transform.position.y + 1.5 < LogicMgr.Instance.GetPlayer().transform.position.y || GetComponentInParent<Enemy>().transform.position.y - 0.5f > LogicMgr.Instance.GetPlayer().transform.position.y)) {
            if (closestLadder == ladder) {
                ladder.GetComponent<Ladder>().EnemyInteract(gameObject);
            }
            return;
        }
        stateTime -= Time.deltaTime;
        if (stateTime <= 0)
        {
            UpdateState(EnemyState.patrolling);
        }

    }

    //enemy has saw the player
    private void ChasePlayer() {
        if (transform.position.y + 1.5 < LogicMgr.Instance.GetPlayer().transform.position.y || transform.position.y > LogicMgr.Instance.GetPlayer().transform.position.y)
        {
            if (ladder != null && ladder == LogicMgr.Instance.GetPlayer().lastLadderUsed)
            {
                if (closeToLadder && closestLadder == ladder) {
                    ladder.GetComponent<Ladder>().EnemyInteract(gameObject);
                }
                else {
                    Heard(ladder.transform.position);
                    goingToLadder = true;
                }
            }
        }

        stateTime -= Time.deltaTime;
        if (stateTime <= 0)
        {
            UpdateState(EnemyState.patrolling);
        }
    }

    //default state
    private void Patrol() {
        timeToTurn -= Time.deltaTime;
        if (timeToTurn <= 0)
        {
            timeToTurn = 5.0f;
            swapDirection();
        }
    }

    //set proper movement according to direction the enemy is facing and world orientation
    private void SetMoveDirection() {
        World.Orientation worldOrientation = LogicMgr.Instance.WorldOrientation();
        if (worldOrientation == currentOrientation) {
            activeAxis = 0;
            leftRight = 1;
        } else {
            if (worldOrientation == World.Orientation.front && currentOrientation == World.Orientation.back
                || worldOrientation == World.Orientation.back && currentOrientation == World.Orientation.front
                ) {
                activeAxis = 0;
                leftRight = -1;
            } else if (worldOrientation == World.Orientation.side && currentOrientation == World.Orientation.front ||
                      worldOrientation == World.Orientation.back && currentOrientation == World.Orientation.side
                  ) {
                activeAxis = 2;
                leftRight = -1;
            } else if (worldOrientation == World.Orientation.front && currentOrientation == World.Orientation.side ||
                      worldOrientation == World.Orientation.side && currentOrientation == World.Orientation.back) {
                activeAxis = 2;
                leftRight = 1;
            }
        }
    }

    
    public void CloseToLadder(bool isClose, GameObject ladder) {        
        if (!isClose) {
            closeToLadder = false;
            closestLadder = null;
        }
        else {
            closeToLadder = true;
            closestLadder = ladder;
        }
    }

    //turn to side where sound was heard and go investigate
    public void Heard(Vector3 position)
    {
        if (aiState == EnemyState.chasing)
            return;

        UpdateState(EnemyState.investigating);




        if (position.x > transform.position.x)
        {
            if (dir == -1)
            {
                swapDirection();
            }
        }

        else if (position.x < transform.position.x)
        {
            if (dir == 1)
            {
                swapDirection();
            }
        }

    }


    public void Chase()
    {

        UpdateState(EnemyState.chasing);

    }

    public float GetSpeed() {
        return speedMultiplier;
    }


    IEnumerator Cooldown(int n) {
        doorTransitCooldown = n;
        while (doorTransitCooldown != 0) {
            doorTransitCooldown--;
            yield return new WaitForSeconds(1);
        }
    }


    void SetLayerRecursively(GameObject obj, int newLayer) {
        obj.layer = newLayer;

        foreach(Transform child in obj.transform) {
            SetLayerRecursively(child.gameObject, newLayer);
        }
    }

    //for transiting through door if chasing player
    IEnumerator TransitDoor(Door door) {
        locked = true;
        GetComponent<Rigidbody>().isKinematic = true;
        while (Vector3.Distance(transform.position, door.targetPlace.position) > 1) {
            transform.position = Vector3.Lerp(transform.position, door.targetPlace.position, Time.deltaTime * 100.0f / 30.0f);
            yield return null;
        }
        transform.position = door.targetPlace.position;
        GetComponent<Rigidbody>().isKinematic = false;
        transform.eulerAngles = transform.eulerAngles + dir * -90f * Vector3.up;
        currentOrientation = World.GetNextOrientation(door.orientation, currentOrientation);
        UpdateState(EnemyState.patrolling);
        locked = false;
        if (currentOrientation == World.Orientation.side && dir == -1)
        {
            dir = 1;
        }
        if(currentOrientation == World.Orientation.side) {
            SetLayerRecursively(gameObject, LayerMask.NameToLayer("Default"));
        }
        else {
            SetLayerRecursively(gameObject, LayerMask.NameToLayer("NotSide"));
        }
        yield return null;
    }

    private void SoundEmitted(Events.EventType receivedEvent) {
        Events.SoundEmitted soundSource = receivedEvent as Events.SoundEmitted;
        Vector3 srcPosition = soundSource.position;
        float radius = soundSource.radius;


        if (hf.GetComponent<CapsuleCollider>().bounds.Contains(srcPosition))
        {
            print("contains");
            Heard(srcPosition);
        }

        //Calculate if enemy can hear the sound etc.
    }

    public void DoorCollider(Door door) {
        Debug.Log("collides with door");
        if (doorTransitCooldown == 0) {
            if (currentOrientation != LogicMgr.Instance.WorldOrientation() && aiState == EnemyState.chasing) {
                Debug.Log("wants to transit door");
                StartCoroutine(Cooldown(3));
                StartCoroutine(TransitDoor(door));
            }
        }

    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player"){
            isKiller = true;
            LogicMgr.Instance.GameOver();
            return;
        }
        
    }

    private void swapDirection()
    {
        dir *= -1;
        transform.eulerAngles = transform.eulerAngles + 180f * Vector3.up;
    }

}
