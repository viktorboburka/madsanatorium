﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StealthHearingField : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && GetComponentInParent<EnemyAI>().getState() != EnemyState.chasing && !LogicMgr.Instance.GetPlayer().GetComponent<PlayerMovement>().stationary)
        {
            //print("stealth heard");
            GetComponentInParent<EnemyAI>().Heard(other.transform.position);
        }
    }
    /*
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            print(other.transform.position.x);
            GetComponentInParent<EnemyAI>().escapedHearing(other.transform.position);

        }
    }*/
}
