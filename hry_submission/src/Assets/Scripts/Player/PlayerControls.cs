﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    Player player;

    //This will be interacted with when LMB pressed
    Interactable interactableInRange = null;
    void Start()
    {
        player = GetComponent<Player>();
    }

    public void AddInteraction(Interactable interactionObject) {
        interactableInRange = interactionObject;
    }
    public void RemoveInteraction() {
        interactableInRange = null;
    }

    public Interactable GetInteractable() {
        return interactableInRange;
    }

    //Solve all key presses   
    void Update()
    {
        if (LogicMgr.Instance.IsPaused() || LogicMgr.Instance.IsGameOver()) {
            if(LogicMgr.Instance.IsGameOver() && LogicMgr.Instance.GetPlayer().movement.isClimbing) {
                (interactableInRange as Ladder).StopClimbing();
            }
            return;
        }

        if (Input.GetMouseButtonDown(0)) {
            if (player.inventory.IsItemSelected()) {
                player.inventory.UseSelectedItem();
            }
            else {
                //INTERACT WITH WORLD
                if(interactableInRange != null) {
                    interactableInRange.Interact();
                }
            }
        }
        if (Input.GetMouseButtonDown(1)) {
            player.flashlight.Switch();
        }
        if (Input.GetKey(KeyCode.LeftShift)) {
            player.movement.TurnStealth(true);
        }
        else {
            player.movement.TurnStealth(false);
        }
        if (Input.GetKeyDown(KeyCode.E)) {
            player.flashlight.ToggleLock();
        }
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            player.inventory.SelectItem(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2)) {
            player.inventory.SelectItem(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3)) {
            player.inventory.SelectItem(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4)) {
            player.inventory.SelectItem(3);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5)) {
            player.inventory.SelectItem(4);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6)) {
            player.inventory.SelectItem(5);
        }
        if (Input.GetKeyDown(KeyCode.Alpha7)) {
            player.inventory.SelectItem(6);
        }
        if (Input.GetKeyDown(KeyCode.Alpha8)) {
            player.inventory.SelectItem(7);
        }
        if (Input.GetKeyDown(KeyCode.Alpha9)) {
            player.inventory.SelectItem(8);
        }
        if (Input.GetKeyDown(KeyCode.Alpha0)) {
            player.inventory.SelectItem(9);
        }
        if (Input.GetKeyDown(KeyCode.Q)) {
            player.inventory.DeselectCurrentItem();
        }
        if (Input.GetKeyDown(KeyCode.Tab)) {
            player.inventory.SelectNextItem();
        }
        if (Input.GetKeyDown(KeyCode.Delete)) {
            LogicMgr.Instance.GameOver();
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            LogicMgr.Instance.Pause();
        }
    }
}
